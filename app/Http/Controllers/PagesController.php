<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index() {
        return View('welcome.index');
    }
    
    public function messages(Request $request){
        $message = new Message();

        $message->sender_name = $request->input('sender_name');
        $message->sender_email = $request->input('sender_email');
        $message->message = $request->input('message');

        $message->save();

        return redirect('/');
    }
}
